from flask import Flask, jsonify, request
from pymongo import MongoClient
import logging
# from logging import FileHandler, WARNING
from logging.handlers import RotatingFileHandler
app = Flask(__name__)

# file_handler = FileHandler("errorlog.txt")
# file_handler.setLevel(logger.info)
# app.logger.addHandler(file_handler)
connection = MongoClient('localhost', 27017)

db = connection['Rest_db']
col = db['entity']
# Display all data in the DB
#  If Data not found  then logger file will updated with lerror messages
@app.route('/entity/', methods=['GET'])
def get_all_frameworks():

    try:
        # colframework = db.col

        output = []

        for q in col.find():
            output.append(
                {'name': q['name'],
                 'description': q['description']
                 }
            )

        return jsonify({'result': output})
    except Exception as error:
        app.logger.error('"No data Found error')
        return jsonify({'Error': str(error)})



#
# @app.route("/log")
# def logTest():
#     app.logger.warning('testing warning log')
#     app.logger.error('testing error log')
#     app.logger.info('testing info log')
#     return "Code Handbook !! Log testing."


# To filter data using name
@app.route('/entity/', methods=['GET'])
def get_one_framework():
    name = request.args.get('name', None)
    print(name)
    try:
        q = col.find_one({'name': name})
        output = {}
        if q:
            output = {'name': q['name'], 'description': q['description']}
        return jsonify({'result': output})
    except Exception as e:
        app.logger.info("No data Found ")

        return jsonify({'Error': str(e)})


# Insert  Data
@app.route('/insert_data/', methods=['POST', 'GET'])
def add_framework():
    output = {}
    if request.method == "POST":
        try:
            name = request.values.get('name')
            de = request.values.get('description')
            print(name)
            col.insert_one({'name': name, 'description': de})
            output = {'name': name, 'description': de}
            return jsonify({'result': output})

        except Exception as e:
            app.logger.info("POST request not Found")
            return jsonify({'Error': str(e)})


# To delete element using name
@app.route('/delete_data/<name>/', methods=['DELETE'])
def delete_one_framework(name):
    print(name)
    try:
        if col.find_one({'name': name}) == None:
            raise Exception
        else:
            col.delete_one({'name': name})
            output = "{} deleted successfully!".format(name)

        return jsonify({'result': output})
    except Exception as e:
        app.logger.error('Name not found in DB')
        return jsonify({'Error': str(e)})


# update data
@app.route('/hello/<name>&<description>/', methods=['PUT'])
# @app.route('/hello/', methods=['PUT'])
def update_framework(name,description):
# def update_framework():
    # col = db['entity']
    # name = request.values.get('name')
    # description = request.values.get('description')
    # print(name)
    # print(description)
    try:
        q = col.find({"_id":name})
        print(q)
        if q:
            val=col.update_one({'name': name}, {'$set': {'description': description}})
        else:
            app.logger.error("Data not Found")
    except Exception as error:
        return jsonify({'Error': str(error)})
    return "Successfully data updated"

if __name__ == "__main__":
    # initialize the log handler
    logHandler = RotatingFileHandler('info.log', maxBytes=1000, backupCount=1)

    # set the log handler level
    logHandler.setLevel(logging.INFO)

    # set the app logger level
    app.logger.setLevel(logging.INFO)

    app.logger.addHandler(logHandler)
    app.run(debug=True)