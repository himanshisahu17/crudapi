import unittest
import requests
import json
import app1


class TestCalc(unittest.TestCase):

    # data = requests.get('http://127.0.0.1:5000/entity/')

    def test_get_all_framework(self):
        response = requests.get(' http://127.0.0.1:5000/entity/')
        self.assertEqual(response.status_code, 200)

    def test_get_one_framework(self):
        response = requests.get(' http://127.0.0.1:5000/entity/?name=parth/')
        self.assertEqual(response.status_code, 200)

    def test_add_framework(self):
        header = {
            "Content-Type": "application/json"
        }
        data = {
            "name": "parth",
            "description": "hello"
        }

        response = requests.post(' http://127.0.0.1:5000/insert_data/', data=json.dumps(data), headers=header)
        self.assertEqual(response.status_code, 200)
        # print(response.text)


    def test_delete_one_framework(self):
        header = {
                    "Content-Type": "application/json"
                }
        data = {"name":"parth"}
        import json
        response = requests.delete(' http://127.0.0.1:5000/delete_data/<goal_name>/', data=json.dumps(data), headers=header)
        self.assertEqual(response.status_code, 200)


    def test_update_framework(self):
        header = {
            "Content-Type": "application/json"
        }
        data = {"name": "parth"},{'$set': {'description': "description"}}
        import json
        response = requests.put('http://127.0.0.1:5000/hello/<name>&<description>/')
        self.assertEqual(response.status_code,200)


if __name__ == '__main__':
        unittest.main()

